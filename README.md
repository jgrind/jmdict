# jmdict

A redistribution of priority entries from the [JMdict] Japanese dictionary.

## License

See [`LICENSE`] for more information.

[`LICENSE`]: LICENSE
[JMdict]: http://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project
